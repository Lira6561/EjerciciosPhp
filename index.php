<?php
	define('DS',DIRECTORY_SEPARATOR);
	define('ROOT',realpath(dirname(__FILE__)).DS);
	
//	require_once "Config/auto.php";
//	Config\auto::run();
//		new Config\request();

?>


<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	<div class="container" style="margin-top: 4em">
		<div class="row px-md-4 mb-4">
		<h1>Curso de php orientado a objetos</h1>
		</div>
		<div class="list-group row">
			
				<a class="list-group-item list-group-item-info list-group-item-action" href="phpejercicios/ejercicio1.php">Ejercicio 1: Clases y objetos</a>
				<a class="list-group-item list-group-item-info list-group-item-action" href="phpejercicios/ejercicio2.php">Ejercicio 2: Métodos y atributos</a>
				<a class="list-group-item list-group-item-info list-group-item-action" href="phpejercicios/ejercicio3.php">Ejercicio 3: Modificador de acceso private</a>
				<a class="list-group-item list-group-item-info list-group-item-action" href="phpejercicios/ejercicio4.php">Ejercicio 4: Atributos con arreglos</a>
				<a class="list-group-item list-group-item-info list-group-item-action" href="phpejercicios/ejercicio5.php">Ejercicio 5: Constructores y destructores</a>
				<a class="list-group-item list-group-item-info list-group-item-action" href="phpejercicios/ejercicio6.php">Ejercicio 6: Herencia</a>
				<a class="list-group-item list-group-item-info list-group-item-action" href="phpejercicios/ejercicio7.php">Ejercicio 7: Herencia y modificador de acceso protected</a>
				<a class="list-group-item list-group-item-info list-group-item-action" href="namespace.php">Prueba del namespace</a>
		</div>
	</div>
</body>
</html>
