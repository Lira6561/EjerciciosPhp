<?php  
//declaracion de clase Abuelos
	class Abuelos{
		//declaracion de atributos protected
		protected $nombreAbuelo='Nemesio';
		protected $apellido_paterno='Martinez';
		protected $apellido_materno='Dominguez';

		protected function crear_ficha(){
			$fichaAbuelo='
					<tr>
						<td>Abuelo:</td>
						<td>'. $this->nombreAbuelo.' '. $this->apellido_paterno.'
						'. $this->apellido_materno.'</td>							
					</tr>';

			return $fichaAbuelo;
		}

	}

//declara una clase Padres que extienda Abuelos
	class Padres extends Abuelos{

		protected $nombrePadre='Jorge';

		public function resumenPadre(){
			$fichaPadre='<tr>
						<td>Padre:</td>
						<td>'. $this->nombrePadre.' '.$this->apellido_paterno.'</td>				
					</tr>
					';
			return $fichaPadre;
		}
	}

//declara una clase Hijo que extienda Padres
	class Hijos extends Padres{

		protected $nombre;

		public function __construct($nombre){
			$this->nombre=$nombre;
		}

		public function resumenHijo(){
			$resumen=parent::crear_ficha();
			$resumen.=$this->resumenPadre();
			$resumen.='<tr>
						<td>Hijo:</td>
						<td>'. $this->nombre.' '.$this->apellido_paterno.'</td>				
					</tr>
					';
			return $resumen;
		}
	
	}

$mensaje='';

if (!empty($_POST)){
	//creacion de objeto de la clase

	$hijo1= new Hijos($_POST['nombre']);
	$mensaje=$hijo1->resumenHijo();
}


?>
<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="/../css/bootstrap.min.css">
	<link rel="stylesheet" href="/../css/bootstrap-grid.css">
	<script type="text/javascript" src="/../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>

	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Familia</h1></header><br>
	<form method="post">
		

					 <div class="form-group">
				 		<label for="CajaTexto1">Escribe tu nombre y descubre de donde vienes:</label>
						<input class="form-control" type="text" name="nombre" id="CajaTexto1">
					</div>
								
		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>

	</div>
	<div class="container mt-5">
		<h1>Respuesta del servidor</h1>
		<table class="table">
			<thead>
		      <tr>
		      	 <th></th>
		      </tr>
		    </thead>
		    <tbody>
			<?= $mensaje; ?>

			</tbody>
		</table>

    </div>

</body>
</html>