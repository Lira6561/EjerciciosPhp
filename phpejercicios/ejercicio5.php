<?php  
//declaracion de clase token
	class token{
		//declaracion de atributos
		private $nombre;
		private $token;
		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
			$this->token=rand();
		}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
		public function mostrar(){
			return 'Hola '.$this->nombre.' este es tu token: '.$this->token;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			//destruye token
			$this->token='El token ha sido destruido';
			echo $this->token;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$token1= new token($_POST['nombre']);
	$mensaje=$token1->mostrar();
}


?>
<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="/../css/bootstrap.min.css">
	<link rel="stylesheet" href="/../css/bootstrap-grid.css">
	<script type="text/javascript" src="/../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	
	<input class='form-control' type='text' value='<?=$mensaje?>' readonly>

	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Recoge tu token</h1></header><br>
	<form method="post">
		

					 <div class="form-group">
				 		<label for="CajaTexto1">Escribe tu nombre:</label>
						<input class="form-control" type="text" name="nombre" id="CajaTexto1">
					</div>

					
			
		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>

	</div>



</body>
</html>