<?php  
//declaracion de clase padre transporte
	class transporte{
		//declaracion de atributos
		private $nombre;
		private $velocidad_max;
		private $tipo_combustible;

		//declaracion de metodo constructor
		public function __construct($nom,$vel,$com){
			$this->nombre=$nom;
			$this->velocidad_max=$vel;
			$this->tipo_combustible=$com;
		}

		//Este metodo genera un ficha en html
		public function crear_ficha(){
			$ficha='
					<tr>
						<td>Nombre:</td>
						<td>'. $this->nombre.'</td>				
					</tr>
					<tr>
						<td>Velocidad máxima:</td>
						<td>'. $this->velocidad_max.'</td>				
					</tr>
					<tr>
						<td>Tipo de combustible:</td>
						<td>'. $this->tipo_combustible.'</td>				
					</tr>';

			return $ficha;
		}
		
	}

	//declaracion de la clase hijo o subclase Carro
	class carro extends transporte{

		private $numero_puertas;

		//declaracion de constructor
		public function __construct($nom,$vel,$com,$pue){
			//sobreescritura de constructor de la clase padre
			parent::__construct($nom,$vel,$com);
			$this->numero_puertas=$pue;
				
		}

		// declaracion de metodo
		public function resumenCarro(){
			// sobreescribitura de metodo crear_ficha en la clse padre
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Numero de puertas:</td>
						<td>'. $this->numero_puertas.'</td>				
					</tr>';
			return $mensaje;
		}
	} 

	class avion extends transporte{

		private $numero_turbinas;
		
		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$tur){
			parent::__construct($nom,$vel,$com);
			$this->numero_turbinas=$tur;
		}

		// sobreescritura de metodo
		public function resumenAvion(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Numero de turbinas:</td>
						<td>'. $this->numero_turbinas.'</td>				
					</tr>';
			return $mensaje;
		}
	}

	class barco extends transporte{
		private $calado;

		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$cal){
			parent::__construct($nom,$vel,$com);
			$this->calado=$cal;
		}

		// sobreescritura de metodo
		public function resumenBarco(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Calado:</td>
						<td>'. $this->calado.'</td>				
					</tr>';
			return $mensaje;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//declaracion de un operador switch
	switch ($_POST['tipo_transporte']) {
		case 'aereo':
			//creacion del objeto con sus respectivos parametros para el constructor
			$jet1= new avion('jet','400','gasoleo','2');
			$mensaje=$jet1->resumenAvion();
			break;
		case 'terrestre':
			$carro1= new carro('carro','200','gasolina','4');
			$mensaje=$carro1->resumenCarro();
			break;
		case 'maritimo':
			$bergantin1= new barco('bergantin','40','na','15');
			$mensaje=$bergantin1->resumenBarco();
			break;		
	}

}

//Front-end
?>
<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="/../css/bootstrap.min.css">
	<link rel="stylesheet" href="/../css/bootstrap-grid.css">
	<script type="text/javascript" src="/../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>

	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Los transportes</h1></header><br>
	<form method="post">
		

					 <div class="form-group">
				 		<label for="CajaTexto1">Tipo de transporte:</label>
						<select class="form-control" name="tipo_transporte" id="CajaTexto1">
							<option value='aereo'>Aereo</option>
							<option value='terrestre'>Terrestre</option>
							<option value='maritimo'>Maritimo</option>
						</select>
					</div>

					
			
		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>

	</div>
	<div class="container mt-5">
		<h1>Respuesta del servidor</h1>
		<table class="table">
			<thead>
		      <tr>
		      	 <th>Transporte</th>
		      </tr>
		    </thead>
		    <tbody>
			<?= $mensaje; ?>

			</tbody>
		</table>

    </div>



</body>
</html>