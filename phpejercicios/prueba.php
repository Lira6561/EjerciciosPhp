<?php
//declaración de clase Mascota que fungirá como clase padre
class Mascota{
    private $nombre;
    public function __construct($nombreMascota){
        $this->nombre=$nombreMascota;
    }
    public function get_nombre(){
        return $this->nombre;
    }
}
//declaracion de subclase Perro que fungirá como clase hija
class Perro extends Mascota{
    public function ladra(){
        //En la subclase se pueden usar los metodos de la clase heredada
        echo $this->get_nombre().": wow wow";
    } 
}
//Cuando se instancia al objeto se debe definir un parametro
$miPerro =new Perro('Firulais');
//ahora llamados al metodo ladrar 
$miPerro->ladra();

    
    
?>